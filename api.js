const express = require('express')
const cors = require('cors')
const jsonServer = require('json-server')
const app = express()
const bodyParser = require('body-parser');
const session = require('express-session');
const fs = require('fs');
const bcrypt = require('bcrypt');
const path = require('path');


let USERS = {};


function saveUserPasswords() {
  let db = JSON.parse(fs.readFileSync('db.json'));
  let dbUsers = (db.users || [])
  let authUsers = getUsers();
  for(let user of dbUsers) {
    let authUserIndex = authUsers.findIndex(u => u.name === user.name)
    if(authUserIndex > -1) {
      user.password = authUsers[authUserIndex].password
    }
  }
  fs.writeFileSync('db.json', JSON.stringify(db, null, 2))
}

function loadUsers() {
  setUsers(JSON.parse(fs.readFileSync('db.json')).users || []);
}

function getUsers() {
  return USERS;
}

function setUsers(users) {
  USERS = users;
}

function getUser(userName) {
  return getUsers().find(u => u.login === userName) || {};
}

function setPassword(userName, password) {
  getUser(userName).password = bcrypt.hashSync(password, 10);
  saveUserPasswords();
}

function getPassword(userName) {
  return getUser(userName).password;
}

function passwordCompareSync(password, passwordHash) {
  if(passwordHash.startsWith('PLAINTEXT=')) {
    return passwordHash.slice(10) === password;
  } else {
    return bcrypt.compareSync(password, passwordHash);
  }
}

function isAdmin(userName) {
  return getUser(userName).isAdmin;
}

function isLoggedIn(req) {
  return req.session.loggedUserName !== undefined;
}

function isLoggedAdmin(req) {
  return isLoggedIn(req) && isAdmin(req.session.loggedUserName);
}

// Serve static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));

app.use(cors({
  credentials: true,
  origin: function (origin, callback) {
    // TODO: Solve for prod/dev environments so it restricts to allowed origins only!
    if (true) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  }
}));
app.use(session({
  secret: 'sad658f4ds65fd4168S4F1D6w41d9f8ew41f6ds5f14684FD16S',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }
}));
app.use(bodyParser.json());
app.use(function(req, res, next) {
  loadUsers();
  next();
})

app.post('/login', function(req, res) {
  let username = req.body.email;
  let password = req.body.password;
  if(getUser(username).login && passwordCompareSync(password, getUser(username).password)) {
    req.session.loggedUserName = username;
    res.send({status: 'OK'});
  } else {
    res.status(403);
    res.send({status: 'Unknown user or incorrect password.'});
  }
});

app.post('/logout', function(req, res) {
  req.session.loggedUserName = undefined;
  res.send();
})

app.get('/user', function(req, res) {
  console.log(req.session)
  if(!isLoggedIn(req)) {
    res.send(JSON.stringify(null));
    return;
  }
  res.send({
    username: req.session.loggedUserName,
    name: getUser(req.session.loggedUserName).name,
    isAdmin: isAdmin(req.session.loggedUserName),
  });
})

app.post('/changePassword', function(req, res) {
  let username = req.body.username || req.session.loggedUserName;
  if(isLoggedAdmin(req)) {
    if(passwordCompareSync(req.body.oldPassword, getUser(req.session.loggedUserName).password)) {
      setPassword(username, req.body.newPassword);
      res.send({status: "OK"});
    } else {
      res.status(403);
      res.send({status: "Invalid old password."});
    }
  } else if(isLoggedIn(req)) {
    if(passwordCompareSync(req.body.oldPassword, getUser(req.session.loggedUserName).password)) {
      if(req.session.loggedUserName === username) {
        setPassword(req.session.loggedUserName, req.body.newPassword);
        res.send({status: "OK"});
      } else {
        res.status(403);
        res.send({status: "Non-admin users can only change password to themselves."})
      }
    } else {
      res.status(403);
      res.send({status: "Invalid old password."});
    }
  } else {
    res.status(403);
    res.send({status: "You need to be logged in to change the password."});
  }
})


app.get('/testicek', (req, res) => {
  res.send({status: "Funguje to."})
})



/*
app.get('/user', (req, res) => {
  res.send(user || 401)
})

app.post('/login', (req, res) => {
  user = {
    username: 'admin',
    name: 'Administrator'
  }
  res.end()
})

app.post('/logout', (req, res) => {
  user = null
  res.end()
})
*/








const router = jsonServer.router('db.json')
app.use(jsonServer.bodyParser)
app.use(router)


// The "catchall" handler: for any request that doesn't
// match one above, send back React's index.html file.
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname+'/client/build/index.html'));
});


const port = process.env.PORT || 8081;
app.listen(port);

console.log('api listening on ' + port)


//app.listen(8081)
