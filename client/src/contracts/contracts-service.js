//import http from '../http'
import http from 'axios'

import _ from 'lodash'
import queryString from 'query-string'

const ContractsService = {
  getContracts() {
    return http.get('/contracts?_expand=customer')
  },

  createContract(data) {
    return http.post('/contracts', data)
  },

  getContract(id) {
    return http.get(`/contracts/${id}?_expand=customer`)
  },

  filterContracts(filter) {
    return http.get(`/contracts?_expand=customer&${queryString.stringify(filter)}`)
  },

  updateContract(contract) {
    return http.put('/contracts/' + contract.id, _.omit(contract, 'customer'))
  },

  deleteContract(id) {
    return http.delete('/contracts/' + id)
  },

  searchContracts(searchText) {
    return http.get('/contracts?_expand=customer&q=' + searchText)
  },

  createContractComment(contractId, comment) {
    comment.contractId = contractId
    
    return http.post('/comments/', comment )  
  },

  getContractComments(contractId) {
    return http.get('/comments/?contractId=' + contractId )
  }
}

export default ContractsService;