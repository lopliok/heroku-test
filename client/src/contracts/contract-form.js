import React from 'react'
import {price} from '../format'


const ContractForm = ({ contract, customers }) => {
  const handleChange = (e) =>
    contract[e.target.name] = e.target.value
    

  return (
    <form>
      <div className="form-row">
        <div className="form-group col-md-4">
          <label className="col-form-label">Name</label>
          <input name="name" type="text" className="form-control" value={contract.name} onChange={handleChange} />
        </div>
        <div className="form-group col-md-4">
          <label className="col-form-label">Price</label>
          <input name="price" type="text" className="form-control currency"  value={price(contract.price)} onChange={handleChange} />
        </div>
      </div>
      <select className="custom-select" name="customerId" value={contract.customerId} onChange={handleChange}>
        <option value="select">Select...</option>
        {customers.map(c =>
          <option key={c.id}  value={c.id}>{c.name}</option>
        )}
      </select>
      <div className="form-group">
        <label className="col-form-label"/>
        <textarea name="description" className="form-control" value={contract.description} onChange={handleChange} />
      </div>

    </form>
  )
}

export default ContractForm
