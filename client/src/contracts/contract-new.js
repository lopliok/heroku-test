import React from 'react'
import ROUTES from '../routes'

import svc from './contracts-service'
import customerService from './../customers/customers-service'

import ContractForm from './contract-form'

class ContractNew extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      contract: {},
      customers: []
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  async load() {
    let res = await customerService.getCustomers()
      this.setState({
        customers: res.data
    })
  }

  create() {
    svc.createContract(this.state.contract).then(res => {
      this.props.history.push(ROUTES.CONTRACT_LISTING)
    })
  }

  render() {
    const contract = this.state.contract
    const customers = this.state.customers

    return (
      <div>
        <h2>Create New</h2>
        <ContractForm contract={contract} customers={customers} />
        <button className="btn btn-primary" onClick={() => this.create()}>Create</button>
      </div>
    )
  }
}

export default ContractNew
