import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'
import ContractsService from './contracts-service'
import _ from 'lodash'
import { price } from '../format'
import timestampToDate from 'timestamp-to-date'

class ContractsListing extends React.Component {

  constructor(props) {

    super(props)

    this.state = {
      contracts: [],
      filter: {
        q: undefined,                 // full-textové vyhledávání
        status: undefined,            // stav zakázky
        dateCreated: undefined,       // datum vytvoření
        dateEdited: undefined,        // datum editace
        deadline: undefined,          // termín dokončení
        dateCreated_gte: undefined,   // datum vytvoření je VĚTŠÍ nebo ROVNO
        dateEdited_gte: undefined,    // datum editace je VĚTŠÍ nebo ROVNO
        deadline_gte: undefined,      // termín dokončení je VĚTŠÍ nebo ROVEN
        dateCreated_lte: undefined,   // datum vytvoření je MENŠÍ nebo ROVNO
        dateEdited_lte: undefined,    // datum editace je MENŠÍ nebo ROVNO
        deadline_lte: undefined       // termín dokončení je MENŠÍ nebo ROVEN
      }      
    }
    
    this.handleFilter = this.handleFilter.bind(this)
    
    this.handleStatusFilter = this.handleStatusFilter.bind(this)
    
    this.loadDebounced = _.debounce(this.load, 200)
  }

  componentWillMount() {
    this.load()
  }

  async load() {
    let res = await ContractsService.filterContracts(this.state.filter)

    this.setState({
      contracts: res.data
    })
  }

  handleFilter(event) {
    let filter = this.state.filter
    filter[event.target.name] = (event.target.value ? event.target.value : undefined)
    this.forceUpdate()
    this.loadDebounced()
  }

  handleStatusFilter(event) {
    let filter = this.state.filter
    filter['status'] = (event.target.value ? event.target.value : undefined)
    this.forceUpdate()
    this.loadDebounced()
  }

  render() {
    let contracts = this.state.contracts
    let filter = this.state.filter

    return (
      <div className="row">
        <div className="col-xs-12">
          <div className="box">
            <div className="box-header">
              <h3 className="box-title">Contracts</h3>
              <div className="box-tools">
                <form onSubmit={this.handleSubmit}>
                  <label>
                    <input name="q" type="text" className="form-control input-sm" placeholder="Search contracts" value={(filter.q ? filter.q : '')} onChange={this.handleFilter} />
                  </label>
                </form>
              </div>
              <div className="box-body">
                <div className="btn-group">
                  <button type="button" className="btn btn-primary" value="NEW" onClick={this.handleStatusFilter}>New</button>
                  <button type="button" className="btn btn-info" value="IN_PROGRESS" onClick={this.handleStatusFilter}>In Progress</button>
                  <button type="button" className="btn btn-success" value="DONE" onClick={this.handleStatusFilter}>Done</button>
                  <button type="button" className="btn btn-danger" value="CANCELED" onClick={this.handleStatusFilter}>Canceled</button>
                  <button type="button" className="btn btn-default" value={undefined} onClick={this.handleStatusFilter}>All</button>
                </div>
              </div>
            </div>
            <div className="box-body">
              <div>
                <div className="row">
                </div>
                <div className="row">
                  <div className="col-sm-12">
                    <table className="table table-bordered table-striped" role="grid">
                      <thead>
                        <tr>
                          <th className="col-sm-2">Title</th>
                          <th>Description</th>
                          <th className="col-sm-1">Price</th>
                          <th className="col-sm-2">Customer</th>
                          <th className="col-sm-1">Status</th>
                          <th className="col-sm-1">Created</th>
                          <th className="col-sm-1">Edited</th>
                          <th className="col-sm-1">Deadline</th>
                        </tr>
                      </thead>
                      <tbody>
                        {contracts.map(s =>
                          <tr key={s.id}>
                            <td><Link to={ROUTES.getUrl(ROUTES.CONTRACT_DETAIL, { id: s.id })}>{s.name}</Link></td>
                            <td>{('' + s.description).slice(0, 60)}</td>
                            <td>{price(s.price)}</td>
                            <td>
                              <Link to={ROUTES.getUrl(ROUTES.CUSTOMER_DETAIL, { id: s.customer.id })}>{s.customer.name}</Link>
                            </td>
                            <td>{s.status}</td>
                            <td>{timestampToDate(s.dateCreated, 'dd.MM.yyyy')}</td>
                            <td>{timestampToDate(s.dateEdited, 'dd.MM.yyyy')}</td>
                            <td>{timestampToDate(s.deadline, 'dd.MM.yyyy')}</td>
                          </tr>
                        )}
                      </tbody>
                    </table>

                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-5">
                    <Link to={ROUTES.CONTRACT_NEW} className="btn btn-app">
                      <i className="fa fa-cart-plus"></i>
                      Create new
                      </Link>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div >

    )
  }
}

export default ContractsListing