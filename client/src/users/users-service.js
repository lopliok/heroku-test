//import http from '../http'
import http from 'axios'

export default {
  getUsers() {
    return http.get('/users')
  },
// rest up to me

  createUser(data) {
    return http.post('/users', data)    
  },

  getUser(id) {
    return http.get('/users/' + id)    
  },

  updateUser(user1) {
    return http.put('/users/' + user1.id, user1)
  },

  changePassword(params) {
    return http.post('/changePassword', params)
  }
}

