import React from 'react'


const UserForm = ({ user1 }) => {
  const handleChange = (e) =>
    user1[e.target.name] = e.target.value

  return (
    <div className="row">
      <div className="col-md-6">
        <form>
            <div className="form-group">
              <label>Login</label>
              <input name="login" type="text" className="form-control" value={user1.login} onChange={handleChange} />
            </div>
            <div className="form-group">
              <label>Name</label>
              <input name="name" type="text" className="form-control" value={user1.name} onChange={handleChange} />
            </div>
        </form>
      </div>
    </div>
  )
}

export default UserForm