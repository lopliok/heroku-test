import React from 'react'
//import { Link } from 'react-router-dom'
import ROUTES from '../routes'

import usersService from './users-service'

import UserForm from './user-form'

class UserEdit extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user1: null
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.load(newProps.match.params.id)
    }
    console.log(newProps)
  }

  load(id) {
    usersService.getUser(id).then(res => {
      this.setState({
        user1: res.data
      })
    })
  }

update() {
  usersService.updateUser(this.state.user1).then(res => {
    this.props.history.push(ROUTES.USER_LISTING)
  })
}

  render() {
    const user1 = this.state.user1

    return (
      <div>
        <h2 className="text-red titlePadding">Edit</h2>
        {user1 && <UserForm user1={user1} />}
        <button className="btn btn-primary" onClick={() => this.update()}>Save</button>
      </div>
    )
  }
}


export default UserEdit