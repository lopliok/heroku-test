import React from 'react'
//import { Link } from 'react-router-dom'
import ROUTES from '../routes'

import usersService from './users-service'

import UserForm from './user-form'

class UserNew extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      user1: {}
    }
  }

create() {
  usersService.createUser(this.state.user1).then(res => {
    this.props.history.push(ROUTES.USER_LISTING)
  })
}

  render() {
    const user1 = this.state.user1

    return (
      <div>
        <h2 className="text-red titlePadding">Create New</h2>
        {user1 && <UserForm user1={user1} />}
        <button className="btn btn-primary" onClick={() => this.create()}>Create</button>
      </div>
    )
  }
}

export default UserNew