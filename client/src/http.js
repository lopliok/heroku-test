import axios from 'axios'

console.log(window.location)

const port = process.env.PORT || 8081;

export default axios.create({baseURL: window.location.protocol + '//' + window.location.hostname + ':' + port, withCredentials: true})
