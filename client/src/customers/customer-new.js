import React from 'react'
import ROUTES from '../routes'

import customersService from './customers-service'

import CustomerForm from './customer-form'

class CustomerNew extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      customer: {}
    }
  }

  create() {
    customersService.createCustomer(this.state.customer).then(res => {
      this.props.history.push(ROUTES.CUSTOMER_LISTING)
    })
  }

  render() {
    const customer = this.state.customer

    return (
      <div>
        <h2 className="text-red titlePadding">Create New</h2>
        <CustomerForm customer={customer} />
        <button className="btn btn-primary" onClick={() => this.create()}>Create</button>
      </div>
    )
  }
}

export default CustomerNew
