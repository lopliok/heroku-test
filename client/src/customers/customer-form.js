import React from 'react'

const CustomerForm = ({ customer }) => {
  const handleChange = (e) =>
    customer[e.target.name] = e.target.value

  return (
    <form>
       <div className="row"> 
        <div className="col-md-6">
          <label className="col-form-label">Name</label>
          <input name="name" type="text" className="form-control" value={customer.name} onChange={handleChange} />
        </div>
        <div className="col-md-6">
          <label className="col-form-label">Email</label>
          <input name="email" type="email" className="form-control" value={customer.email} onChange={handleChange} />
        </div>
        <div className="col-md-6">
          <label className="col-form-label">Phone</label>
          <input name="phone" type="text" className="form-control" value={customer.phone} onChange={handleChange} />
        </div>
        <div className="col-md-6">
          <label className="col-form-label">City</label>
          <input name="city" type="text" className="form-control" value={customer.city} onChange={handleChange} />
        </div>
        <div className="col-md-12">
        <label className="col-form-label">Note</label>
        <textarea name="note" className="form-control" value={customer.note} onChange={handleChange} />
        </div>
      </div>
    </form>
  )
}

export default CustomerForm
