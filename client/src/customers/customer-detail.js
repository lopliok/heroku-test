import React from 'react'
import { Link } from 'react-router-dom'
import ROUTES from '../routes'

import customersService from './customers-service'

import Collapsible from '../collapsible'

class CustomerDetail extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      customer: {}
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.load(newProps.match.params.id)
    }
  }

  async load(id) {
    const res = await customersService.getCustomer(id)

    this.setState({
      customer: res.data
    })
  }

  render() {
    const customer = this.state.customer

    return (
      <div>
        <h2 className="text-red titlePadding">Detail</h2>
        <div className="btn-group margin-bottom">
          <Link className="btn btn-default" to={ROUTES.getUrl(ROUTES.CUSTOMER_EDIT, { id: customer.id })}>Edit</Link>
          <Link className="btn btn-danger" to={ROUTES.getUrl(ROUTES.CUSTOMER_LISTING)}>Delete</Link>
        </div>

        <table className="table table-condensed">
          <tbody>
            <tr>
              <th className="noTopBorder w4">Name</th>
              <td className="noTopBorder">{customer.name}</td>
            </tr>
            <tr>
              <th className="noTopBorder">E-mail</th>
              <td className="noTopBorder">{customer.email}</td>
            </tr>
            <tr>
              <th className="noTopBorder">Phone</th>
              <td className="noTopBorder">{customer.phone}</td>
            </tr>
            <tr>
              <th className="noTopBorder">City</th>
              <td className="noTopBorder">{customer.city}</td>
            </tr>
            <tr>
              <th className="noTopBorder">Note</th>
              <td className="noTopBorder">
                <Collapsible>
                  {customer.note}
                </Collapsible>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    )
  }
}


export default CustomerDetail
