import React from 'react'
import ROUTES from '../routes'

import customersService from './customers-service'

import CustomerForm from './customer-form'

class CustomerEdit extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      customer: null
    }
  }

  componentWillMount() {
    this.load(this.props.match.params.id)
  }

  componentWillReceiveProps(newProps) {
    if (this.props.match.params.id !== newProps.match.params.id) {
      this.load(newProps.match.params.id)
    }
  }

  load(id) {
    customersService.getCustomer(id).then(res => {
      this.setState({
        customer: res.data
      })
    })
  }

  update() {
    customersService.updateCustomer(this.state.customer).then(res => {
      this.props.history.push(ROUTES.CUSTOMER_LISTING)
    })


  }

  render() {
    const customer = this.state.customer

    return (
      <div>
        <h2 className="text-red titlePadding">Edit</h2>
        {customer && <CustomerForm customer={customer} />}
        <button className="btn btn-primary" onClick={() => this.update()}>Save</button>
      </div>

    )
  }
}

export default CustomerEdit
