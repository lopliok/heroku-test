import React from 'react'

import authService from './auth-service'

class Login extends React.Component {
  
  constructor(props) {
    super(props)

    this.state = {
      credentials: {
          "email": "",
          "password": ""
      }
    }
  }
  
  render() {
  
    const credentials = this.state.credentials
  
    const handleChange = (e) => {
      credentials[e.target.name] = e.target.value
      this.forceUpdate()
    }
  
    return (
      <div className="login-page">
        <div className="login-box">
          <div className="login-logo">
            <b>Customer</b> APP
        </div>
          <div className="login-box-body">
            <p className="login-box-msg">Please login</p>
            <div className="form-group has-feedback">
              <input name="email" value={credentials.email} type="email" className="form-control" placeholder="Email" onChange={handleChange} />
              <span className="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            {/* <input type="text" className="form-control" name="username" placeholder="Email Address" required="" autoFocus/>
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span> */}
            <div className="form-group has-feedback">
              <input name="password" value={credentials.password} type="password" className="form-control" placeholder="Password" onChange={handleChange} />
              <span className="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            {/* <input type="password" className="form-control" name="password" placeholder="Password" required=""/> */}
            <div className="row">
              {/* <label className="col-xs-8">
              <input type="checkbox" value="remember-me" name="rememberMe"/>
              Remember me
            </label> */}
    
              <div className="col-xs-8">
                <div className="checkbox icheck">
                  <label className="">
                    <div className="icheckbox_square-blue" aria-checked="false" aria-disabled="false" style={{ position: 'relative' }}>
                      <input type="checkbox" />
                      <ins className="iCheck-helper"></ins>
                    </div>
                    Remember Me
                  </label>
                </div>
              </div>
    
              <div className="col-xs-4">
                <button className="btn btn-lg btn-primary btn-block" type="submit" onClick={() => authService.login(credentials)}>Login</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Login
