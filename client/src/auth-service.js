// import http from './http'
import http from 'axios'

export default {
  user: null,

  onChange: null,

  async init() {
    this.reload()
  },

  async reload() {
    try {
      const res = await http.get('/user')
      this.user = res.data      
    } catch (e) {
      this.user = null
    }

    this.onChange()
  },

  isLoggedIn() {
    return this.user !== null
  },

  async login(credentials) {
    await http.post('/login', credentials)
    this.reload()
  },

  async logout() {
    await http.post('/logout')
    this.reload()
  }
}
