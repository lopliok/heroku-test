const priceFormat = new Intl.NumberFormat('cs-CS',{ style: 'currency',currency:'CZK'})

export const price = (v) =>
  isNaN(v) ?v :priceFormat.format(v)