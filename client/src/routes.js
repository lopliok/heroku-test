export default {
  DASHBOARD:  '/dashboard',
  
  CUSTOMER_LISTING: '/customers',
  CUSTOMER_NEW: '/customers/new',
  CUSTOMER_DETAIL: '/customers/detail/:id',
  CUSTOMER_EDIT: '/customers/edit/:id',

  USER_LISTING: '/users',
  USER_NEW: '/users/new',
  USER_DETAIL: '/users/detail/:id',
  USER_EDIT: '/users/edit/:id',
  USER_PASSWORD_CHANGE: '/change-password/:id',

  CONTRACT_LISTING: '/contracts',
  CONTRACT_NEW: '/contracts/new',
  CONTRACT_DETAIL: '/contracts/detail/:id',
  CONTRACT_EDIT: '/contracts/edit/:id',

    getUrl(path, params = {}) {
      return path.replace(/:(\w+)/g, (m, k) => params[k])
    }
}
