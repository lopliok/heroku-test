//import http from '../http'
import http from 'axios'
export default {
  async getCustomersCount() {
    const value = await http.get('/customers')
    return value.data.length
  },

  async getContractsCount() {
    const value = await http.get('/contracts')
    return value.data.length
  }
}