//import http from '../http'
import http from 'axios'
import _ from 'lodash'


export default {
    async getStatusData() {
        const value = await http.get('/contracts')
    
        var countData = _.countBy(value.data, "status")
       
        var contactStatusData = [
            { status: 'All', count: value.data.length },
            { status: 'Deal', count: countData.IN_PROGRESS + countData.DONE },
            { status: 'Done', count: countData.DONE },
        ]
        return contactStatusData
    }
}